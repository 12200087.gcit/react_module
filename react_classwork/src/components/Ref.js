import React, { useRef, useState } from 'react';

function Form() {
  const inputRef = useRef(null);
  const selectRef = useRef(null);
  const[name, setname] = useState('')
  const[course, setCourse] = useState('')


  const handleSubmit = (event) => {
    event.preventDefault();
    // handlename()
    setname(inputRef.current.value)
    setCourse(selectRef.current.value)
    console.log('Name :', inputRef.current.value);
    console.log('Your Programme :', selectRef.current.value);
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Your Name :
        <input type="text" ref={inputRef}/>
      </label>
      <label>
        Choose your Programme :
        <select ref={selectRef}>
          <option value=" ">Choose Programme</option>
          <option value="Bsc.IT">Bsc.IT</option>
          <option value="Bsc.CS">Bsc.CS</option>
          <option value="Blockchain">Blockchain</option>
        </select>
      </label>
      <button type="submit">Submit</button>
      <p>{name}</p>
      <p>{course}</p>
      
    </form>
  );
}

export default Form;