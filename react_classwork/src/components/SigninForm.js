import React from 'react'
import styles from './SigninForm.module.css'
export default function SigninForm() {
    return (
        <div className={styles.sign}>
          <h1 className={styles.h1} >Sign In</h1>
          <form className={styles.form}>
            <input className={styles.input} placeholder='Username' type="username"  name="username" required />
            <input type="password" placeholder='Password' name="password" required/>
            <button className={styles.login} type="submit">Login</button>
          </form>
        </div>
      );
}
