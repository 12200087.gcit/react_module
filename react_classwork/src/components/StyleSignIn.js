import React from 'react';
import styled from 'styled-components';

const StyledSignIn = () => {
    const Container=styled.div`
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        height: 100vh;
        background-color: #f9f9f9;
        border: 1px solid #cccccc;
    `
    const Header=styled.h1`
         margin-right: 230px;
         color: black
    `
    const FormSignIn=styled.form`
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        width: 300px;
        background-color: #ffffff;
        padding: 20px;
        border-radius: 10px;
        border: 1px solid #cccccc;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
    `
    const LoginFiled=styled.input`
        width: 100%;
        height: 30px;
        border: 1px solid #cccccc;
        border-radius: 5px;
        padding: 5px;
        margin-bottom:15px`
    const Button=styled.button`
        color:#ffffff;
        width:25%;
        height: 40%;
        border-radius:8px;
        background-color:#AED7E5;
        border:#ffffff
    `    
  return (
    <Container>
      <Header  >Styled SignIn</Header>
      <FormSignIn >
        <LoginFiled  placeholder='Username' type="username"  name="username" required />
        <LoginFiled type="password" placeholder='Password' name="password" required/>
        <Button  type="submit">Login</Button>
      </FormSignIn>
    </Container>
  );
};

export default StyledSignIn;