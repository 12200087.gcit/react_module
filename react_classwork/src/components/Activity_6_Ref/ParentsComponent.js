import { useState } from "react";
import React from "react";
import ChildCom from "./ChildComponent";

function ParentComponent(){
    const [name, setName] = useState('');
    const [course, setCourse] = useState('');
   
  const Submit = (inputRef, selectRef)=> {
    console.log('Your name:', inputRef);
    console.log('Course:', selectRef);
    setName(inputRef)
    setCourse(selectRef)
    
  };
  return(
    <>
        <ChildCom onFormSubmit={Submit}/>
    </>
  )
}

export default ParentComponent;