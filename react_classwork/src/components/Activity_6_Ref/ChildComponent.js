import React, { useRef, useState } from 'react';

function ChildComponent() {
  const inputRef = useRef(null);
  const selectRef = useRef(null);
  const[name, setname] = useState('')
  const[course, setCourse] = useState('')

  function handleSubmit(event) {
    event.preventDefault();
    setname (inputRef.current.value)
    setCourse (selectRef.current.value)
    console.log(`Your Name: ${name}`);
    console.log(`Your Course: ${course}`);
  }

  return (
    <>
      <form onSubmit={handleSubmit}>
        <div style={{
                width: '57%',
                // marginLeft: '10rem',
                margin:'15rem',
                padding:'2rem',
                border: '1px solid #CCCCCC'}}>
            <label htmlFor="input-field">Your Name:</label> <br />
            <input id="input-field" type="text" ref={inputRef} />
            <br />
            <label htmlFor="select-menu">Your Program:</label> <br />
            <select id="select-menu" ref={selectRef} placeholder='Choose from here'>
                <option >Choose course</option>
                <option >Bsc.CS</option>
                <option >Bsc.IT</option>
                <option >Blockchain</option>
            </select>
            <br />
            <button type="submit">Submit</button>
            <p>{name}</p>
            <p>{course}</p>
        </div>
       
      </form>
    </>
  );
}

export default ChildComponent;
