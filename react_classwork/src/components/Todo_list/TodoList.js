import React from "react";
import TodoItem from "./TodoItem";


function TodoList(props) {
return (
<div>
    <div>

        <button  style={{ margin:'10px',}}
            onClick={() => props.toggleDisplayCompleted(false)}>Show Uncompleted
        </button>
        
        <button style={{ margin:'10px',}} 
            onClick={() => props.toggleDisplayCompleted(true)}>Show Completed
        </button>

        <button style={{margin:'10px'}}  
            onClick={() => props.clearCompleted(false)}>Clear Completed
        </button>

    </div>
    <div className="todo-list">
        {props.todoList
        .filter((todo) => todo.completed === props.displayCompleted)
        .map((todo) => (
        <TodoItem
                key={todo.id}
                id={todo.id}
                text={todo.text}
                completed={todo.completed}
                toggleCompleted={props.toggleCompleted}
                />
        ))}
    </div>
</div>
);
}

export default TodoList;