import React from "react";

function TodoItem(props) {

  return (
    <div
    className={`todo-item ${props.completed ? " completed" : ""}`}
    onClick={() => props.toggleCompleted(props.id)}>

      <span >    
        <li style={{marginLeft:'20px'}}>
          {props.text}
         </li>   
      </span>

    </div>
  );
}

export default TodoItem;