import React from "react";
import ReactDOM from "react-dom";
import './modal.css'

export default function Modal({ show, closeModal }) {
  if (!show) {
    return null;
  } else {
    return ReactDOM.createPortal(
      <div className="modal">
        <div className="overlay" onClick={closeModal}></div>
        <div className="content">
          <p style={{ color: "white" }}>Dashboard</p>
          <p style={{ color: "white" }}>All Courses</p>
          <p style={{ color: "white" }}>Course Detail</p>
          <div />
        </div>
      </div>,
      document.getElementById("portal")
    );
  }
}
