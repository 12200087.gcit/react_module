import React from 'react'
import styles from './SignUp.module.css'

export default function SignupForm() {
    return (
        <div className={styles.sign}>
          <h1 className={styles.h1}>Sign Up</h1>
          <form className={styles.form}>
          
            <input type="text" placeholder='Username' name="name" required />
            <input type="email" placeholder='Email' name="email" required />
            <input type="password" placeholder='Password' name="password" required />
            <input type="password" placeholder='ConfirmPassword' name="password" required />
            <button className={styles.Sign} type="submit">Sign Up</button>
          </form>
        </div>
      );
}
