import React, { useState,useEffect } from "react";
import './form.css'


function NewPost() {
    const[data,setData]=useState([])
    const[input,setInput]=useState([])
    const [isSendingRequest, setIsSendingRequest] = useState(false);
    useEffect(()=>{
        fetch('https://jsonplaceholder.typicode.com/posts')
        .then(response=>response.json())
        .then(data=>setData(data))
        .catch(error=>{
            console.log('error')
        })
        
    })
   

    async function submitHandler(event) {
      event.preventDefault();
  
      setIsSendingRequest(true);
  
      const response = await fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          title: event.target.title.value,
          body: event.target.body.value,
          userId: 1,
        }),
      });
  
      setIsSendingRequest(false);
  
      if (!response.ok) {
        throw new Error("HTTP error, status = " + response.status);
      }
  
      event.target.title.value = "";
      event.target.body.value = "";
    }
 
  return (
 
  
  
    <div class="container-sm">
        
      <div class="mb-3">
        
        
                <form class="form" onSubmit={submitHandler}>
                <label for="exampleFormControlInput1" class="form-label">Title</label>
                    <div class="input-container">
                    <input type="text" value={input} onChange={e=>setInput(e.target.value)}/>
                    <span>
                    </span>
                </div>
                <button type="submit">
                {isSendingRequest ? "Saving..." : "Save"}
                </button>
            </form>
            {/* <p>{data}</p> */}
            <div class=" mx-3" style={{boxShadow:'rgba(0, 0, 0, 0.24) 0px 3px 8px'}}>
          
            {
                data.map(item=>(
                    <div key={item.id} style={{width:'70rem'}} >
                      
                        <p class="card-text"
                        style={{textAlign:'left', marginLeft:'2rem'}}>{item.body}</p>
                    </div>

                ))
            }
            </div>
        </div>
        
    </div>
  );
}

export default NewPost;
