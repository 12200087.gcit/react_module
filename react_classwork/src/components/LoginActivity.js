
import React, { useState } from 'react';
export default function LoginActivity() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailError, setEmailError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);
  

  function handleSubmit(e) {
    e.preventDefault();
    // Perform form submission logic here
  }

  function handleEmailChange(e) {
    setEmail(e.target.value);
    setEmailError(!isValidEmail(e.target.value));
  }

  function handlePasswordChange(e) {
    setPassword(e.target.value);
    setPasswordError(!isValidPassword(e.target.value));
  }

  function isValidEmail(email) {
    // Email validation logic here
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  }

  function isValidPassword(password) {
    // Password validation logic here
    const hasLowerCase = /[a-z]/.test(password);
    const hasUpperCase = /[A-Z]/.test(password);
    const hasNumeric = /[0-9]/.test(password);
    const hasSpecialChar = /[!@#$%^&*(),.?":{}|<>]/.test(password);
    const hasMinimumLength = password.length >= 8;

    // Check if all rules are met
    return (
        hasLowerCase &&
        hasUpperCase &&
        hasNumeric &&
        hasSpecialChar &&
        hasMinimumLength
      );
  }
  let x='Tandin'
  console.log(x)

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor="email" style={{ color: emailError ? 'red' : 'black' }}>
          Email Address
        </label><br/>
        <input
          type="email"
          id="email"
          value={email}
          onChange={handleEmailChange}
          style={{
            backgroundColor: emailError ? 'red' : 'white',
            borderColor: emailError ? 'red' : 'white'
          }}
        />
      </div>
      <div>
        <label htmlFor="password" style={{ color: passwordError ? 'red' : 'black' }}>
          Password
        </label><br/>
        <input
          type="password"
          id="password"
          value={password}
          onChange={handlePasswordChange}
          style={{
            backgroundColor: passwordError ? 'red' : 'white',
            borderColor: passwordError ? 'red' : 'white'
          }}
        />
      </div>
      <p></p>
      <button type="submit">Submit</button>
    </form>
  );
}


