
import { useState } from "react";
import Modal from "../src/components/Activity--7/modal";
import "../src/components/Activity--7/modal.css";
import "../src/components/Activity_8/NewPost"
import NewPost from "../src/components/Activity_8/NewPost";

export default function Activity7() {
  const [show, setShow] = useState(false);
  return (
    <div className="App">
      <div
        style={{
          background: "green",
          position: "absolute",
          top: 0,
          left: 0,
          width: "100%",
          display: "flex",
          height: "15vh",
          alignItems: "center"
        }}
      >
        <h1 style={{ color: "white", paddingLeft: "10px" }}>Demo App</h1>
        <svg
          viewBox="0 0 24 24"
          xmlns="http://www.w3.org/2000/svg"
          width="24"
          height="24"
          onClick={() => setShow((s) => !s)}
          style={{
            marginLeft: "auto",
            paddingRight: "10px"
          }}
        >
          <path
            fill="white"
            d="M2,6h20v2H2V6z M2,11h20v2H2V11z M2,16h20v2H2V16z"
          />
        </svg>
      </div>

      <Modal show={show} closeModal={() => setShow(false)} />
      {/* <NewPost></NewPost> */}
    </div>
  );
}

